import java.util.Scanner;

public class AB_Array_einfache_Uebungen {

	static Scanner tastatur = new Scanner(System.in);
	

	public static void main(String[] args) {
		
		int zahl = 0;
		
		
		System.out.println("Welche Aufgabe?: ");
		System.out.println("0 zum beenden. ");
		
		zahl=tastatur.nextInt();
		
		switch(zahl) {
		
		case 0:
			System.exit(0);
		case 1:
			aufgabe1();
			break;
		case 2:
			aufgabe2();
			break;
		case 3:
			aufgabe3();
			break;
		case 4:
			aufgabe4();
			break;
			
		default:
			System.out.println("Falsche Eingabe, es geht nur bis Aufgabe 4");
		
		}

	}

	public static void aufgabe1() {
		
	}
	public static void aufgabe2() {
		
	}
	public static void aufgabe3() {
	
	}
	public static void aufgabe4() {
	
	}
}
