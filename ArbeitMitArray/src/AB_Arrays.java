import java.util.Random;
import java.util.Scanner;

public class AB_Arrays {
	
	static Random random = new Random();
	static Scanner tastatur = new Scanner(System.in);

	public static void main(String[] args) {
		
		int[] myArray = new int[10];
		
		
		for(int i = 0; i <10; i++) {
			myArray[i]=random.nextInt();}
		
		System.out.println("10 Zufallszahlen:");
		for(int i = 0; i <10; i++) {
			System.out.println(myArray[i]);}
		
		
		myArray[3] = 1000;
		System.out.println("");
		System.out.println("Wert des Index 3");
		System.out.println(myArray[3]);
		
		
		for(int i = 0; i <10; i++) {
			myArray[i]=0;}
		
		
		
		String aufgabe;
		int auswahl=0;
		int wert = 0;
		
		do {
			System.out.println("Welche Aufgabe?: ");
			System.out.println("-------------------------------");
			System.out.println("a. Alle Werte ausgeben");
			System.out.println("b. Einen bestimmten Wert ausgeben");
			System.out.println("c. Das Array komplett mit neuen Werten bef�llen");
			System.out.println("d. Einen bestimmten Wert �ndern");
			System.out.println("0 zum beenden. ");
			
			
			
			aufgabe=tastatur.next();
			
			switch(aufgabe) {
			
			case "0":
				System.exit(0);
				
			case "a":
				for(int i = 0; i <10; i++) {
					System.out.println(myArray[i]);}
				break;
				
			case "b":
				System.out.println("Welchen Wert m�chten Sie ausgeben? (1-10)");
				auswahl = tastatur.nextInt();
				
				System.out.println(myArray[auswahl-1]);
				break;
				
			case "c":
				for(int i = 0; i <10; i++) {
					myArray[i]=random.nextInt();}
				break;
				
			case "d":
				System.out.println("Welchen Wert m�chten Sie �ndern? (1-10)");
				auswahl=tastatur.nextInt();
				System.out.println("Was ist der neue Wert? ");
				wert=tastatur.nextInt();
				myArray[auswahl-1]=wert;
				break;
				
				
			default:
				System.out.println("Falsche Eingabe, es geht nur bis Aufgabe 4");

			}
			
		}while(true);
			
	
	}
}
