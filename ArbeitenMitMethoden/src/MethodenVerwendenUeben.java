import java.util.Scanner;

public class MethodenVerwendenUeben {
	
	static Scanner myScanner = new Scanner(System.in);
	
	static String artikel;
	static int anzahl;
	static double preis;
	static double mwst;
	
	static double nettogesamtpreis;
	static double bruttogesamtpreis;

	public static void main(String[] args) {
		
		Benutzereingaben();
		Verarbeiten();
		Ausgeben();

		
	}
	public static void Benutzereingaben() {
		System.out.println("was m�chten Sie bestellen?");
		artikel = myScanner.next();

		System.out.println("Geben Sie die Anzahl ein:");
		anzahl = myScanner.nextInt();

		System.out.println("Geben Sie den Nettopreis ein:");
		preis = myScanner.nextDouble();

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		mwst = myScanner.nextDouble();
		return;
	}
	
	public static void Verarbeiten() {
		nettogesamtpreis = anzahl * preis;
		bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return;
	}
	
	public static void Ausgeben() {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		return;
	}

}