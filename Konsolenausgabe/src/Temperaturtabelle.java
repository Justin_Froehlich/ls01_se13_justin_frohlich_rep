
public class Temperaturtabelle {

	public static void main(String[] args) {
		/*
		 * Ubungsblatt2 - Aufgabe 3
		 * 
		 * 29.09.2021
		 * @Justin Fr�hlich
		 */
		
		int fahrenheit1 = -20;
		int fahrenheit2 = -10;
		int fahrenheit3 = +0;
		int fahrenheit4 = +20;
		int fahrenheit5 = +30;
		
		double celsius1 = -28.8889;
		double celsius2 = -23.3333;
		double celsius3 = -27.7778;
		double celsius4 = -6.6667;
		double celsius5 = -1.1111;
		
		String punkte = ".........................";
		String tabellenkopf1 = "Fahrenheit";
		String tabellenkopf2 = "Celsius";
		
		
		
		System.out.printf("%-12s | %10s\n%s\n", tabellenkopf1, tabellenkopf2, punkte);
		
		
		
		
		
		System.out.printf("%-12d | %10.2f\n", fahrenheit1, celsius1);
		System.out.printf("%-12d | %10.2f\n", fahrenheit2, celsius2);
		System.out.printf("%-12d | %10.2f\n", fahrenheit3, celsius3);
		System.out.printf("%-12d | %10.2f\n", fahrenheit4, celsius4);
		System.out.printf("%-12d | %10.2f\n", fahrenheit5, celsius5);
		
	}

}
