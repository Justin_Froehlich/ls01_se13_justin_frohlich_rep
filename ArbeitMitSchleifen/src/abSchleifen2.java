import java.util.Scanner;

public class abSchleifen2 {

	static Scanner tastatur = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		aufgabe2();
	}
	
	
	public static void aufgabe1() {
		int zahl = 0;
		System.out.println("Geben sie eine Zahl ein:");
		zahl = tastatur.nextInt();
		
		int i = 1;
		System.out.print("Vorw�rts:  ");
		while(i <= zahl) {
			System.out.print(i + " ");
			i++;
		}
		System.out.println("");
		
		System.out.print("R�ckw�rts: ");
		while(zahl >= 1) {
			System.out.print(zahl + " ");
			zahl--;
		}
		System.out.println("");
	}

	public static void aufgabe2() {
		boolean eingabeKorrekt = false;
	    int zaehler = 1;
	    long ausgabe = 1;
	    int zahl=0;
	    
	    

	    while (eingabeKorrekt == false) {
	    System.out.println("Geben sie eine Zahl ein: (0-20)");
	    zahl = tastatur.nextInt();
	    if(zahl <= 20) {eingabeKorrekt = true;}
	    else {System.out.println("falsche Eingabe, bitte erneut probieren");}
	    }

	    do {
	        ausgabe = ausgabe * zaehler;
	        zaehler++;
	    }while (zaehler <= zahl);

	    System.out.println(ausgabe);
	}
	
}
