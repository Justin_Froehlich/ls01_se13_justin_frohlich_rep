import java.util.Scanner;

public class abSchleifen1 {

	
	static Scanner tastatur = new Scanner(System.in);
	
	public static void main(String[] args) {
		aufgabe2();


	}

	
	public static void aufgabe1() {
		int zahl = 0;
	    System.out.println("Geben sie die Zahlengrenze ein:");
	    zahl = tastatur.nextInt();

	    //a)
	    for(int i=1;i<=zahl;i++)    {
	        System.out.print(i + " ");
	    }

	    System.out.println("");

	    //b)
	    for(int i=0;zahl>=1;zahl--)    {
	        System.out.print(zahl + " ");
	    }
	    System.out.println("");
	}
	
	public static void aufgabe2() {
		
		 int zahl = 0;
		    System.out.println("Geben sie eine Zahl ein:");
		    zahl = tastatur.nextInt();
		    int zaehler = 0;
		    int berechnungsZahl = 1;

		    //a)
		    for(int i = 0;i<zahl;i++) {
		        zaehler = zaehler + berechnungsZahl;
		        berechnungsZahl ++;
		    }
		    System.out.println(zaehler);

		    berechnungsZahl = 0;
		    zaehler = 0;

		    //b)
		    for(int i = 0;i<zahl;i++) {
		        zaehler = zaehler + berechnungsZahl;
		        berechnungsZahl = berechnungsZahl + 2;
		    }

		    zaehler = zaehler + (2*zahl);

		    System.out.println(zaehler);

		    berechnungsZahl = 1;
		    zaehler = 0;

		    //c)
		    for(int i = 0;i<zahl;i++) {
		        zaehler = zaehler + berechnungsZahl;
		        berechnungsZahl = berechnungsZahl + 2;
		    }

		    zaehler = zaehler + (2*zahl+1);

		    System.out.println(zaehler);

		    }
}

	
	
