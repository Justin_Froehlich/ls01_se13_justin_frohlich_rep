import java.util.Scanner;

public class ab_Verzweigungen_1 {
	
	static Scanner tastatur = new Scanner (System.in);

	public static void main(String[] args) {
		int auswahl;
		System.out.println("Welche Aufgabe m�chten sie? (1-5)");
		auswahl = tastatur.nextInt();
		
		if (auswahl == 1) {aufgabe1();}
		if (auswahl == 2) {aufgabe2();}
		if (auswahl == 3) {aufgabe3();}
		if (auswahl == 4) {aufgabe4();}
		if (auswahl == 5) {aufgabe5();}

	}
	
//	################################################################################################################### //
	
	public static void aufgabe1() {
		int wert1;
		int wert2;
		
		System.out.println("--- Vergleichen von zwei Werten ---");
		
		System.out.println("Geben Sie den ersten Wert an:");
		wert1 = tastatur.nextInt();
		
		System.out.println("Geben Sie den zweiten Wert an:");
		wert2 = tastatur.nextInt();
		
		if (wert1==wert2) {
			System.out.println("Gleiche Werte");}
		else {
			System.out.println("Ungleiche Werte");}
	}
	
//	################################################################################################################### //
	
	public static void aufgabe2() {
		double zahl1, zahl2;
		
		System.out.println("--- Vergleichen von zwei reelen Zahlen ---");
		
		System.out.println("Geben Sie den ersten Wert an:");
		zahl1 = tastatur.nextDouble();
		
		System.out.println("Geben Sie den zweiten Wert an:");
		zahl2 = tastatur.nextDouble();
		
		if (zahl1==zahl2) {
			System.out.printf("%.2f %.2f", zahl1, zahl2);}
		else {
			if (zahl1 < zahl2) {System.out.printf("%.2f \n%.2f", zahl1, zahl2);}
			if (zahl2 < zahl1) {System.out.printf("%.2f \n%.2f", zahl2, zahl1);}
			}
	}
	
//	################################################################################################################### //
	
	public static void aufgabe3() {
		double zahl;
		
		System.out.println("--- �berpr�fen des Vorzeichens von Zahlen ---");
		
		System.out.println("Geben Sie die zu �berpr�fenden Zahl ein.");
		zahl = tastatur.nextDouble();
		
		if (zahl < 0) {
			System.out.println("Die Zahl ist negativ");}
		if (zahl >= 0) {
			System.out.println("Die Zahl ist positiv");}
	}

//	################################################################################################################### //
	
	public static void aufgabe4() {
		double zahl;
		
		System.out.println("--- �berpr�fung der Gr��e der Zahlen ---");
		
		System.out.println("Geben Sie eine Zahl zwischen 0 und 100 ein (einschlie�lich Grenzen):");
		zahl = tastatur.nextDouble();
		
		if (zahl >= 0 && zahl <= 100) {
			if (zahl > 50) {
				System.out.println("Die Zahl ist gro�");}
			if (zahl <= 50) {
				System.out.println("Die Zahl ist klein");}
		}
		
		else {
			System.out.println("Fehlerhafte Eingabe");}
	}

//	################################################################################################################### //
	
	public static void aufgabe5() {
		double zahl;
		
		System.out.println("--- �berpr�fung der Gr��e der Zahlen ---");
		
		System.out.println("Geben Sie eine Zahl ein");
		zahl = tastatur.nextDouble();
		
		if (zahl%2 == 0 ) {zahl=zahl/2;}
		else {System.out.println("Zahl ungerade");}
		
	}
}
