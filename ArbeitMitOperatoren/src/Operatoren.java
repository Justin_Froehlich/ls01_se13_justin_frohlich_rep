/* Operatoren.java
   Uebung zu Operatoren in Java
*/
public class Operatoren {
      public static void main(String[] args) {
            /* 1. Deklarieren Sie zwei Ganzzahlen.*/
    	  
    	  int zahl1;
    	  int zahl2;
    	  int ergebnis;

            System.out.println("UEBUNG ZU OPERATOREN IN JAVA");
            System.out.println();

            /* 2. Weisen Sie den Ganzzahlen die Werte 75 und 23 zu
                  und geben Sie sie auf dem Bildschirm aus. */
            
            zahl1 = 75;
            zahl2 = 23;
            
            System.out.println("Zahl 1: " + zahl1);
            System.out.println("Zahl 2: " + zahl2);

            /* 3. Addieren Sie die Ganzzahlen
                  und geben Sie das Ergebnis auf dem Bildschirm aus. */
            
            ergebnis = zahl1 + zahl2;
            System.out.println(zahl1 + " + " + zahl2 + " = " + ergebnis);

            /* 4. Wenden Sie *alle anderen* arithmetischen Operatoren auf die
                  Ganzzahlen an und geben Sie das Ergebnis jeweils auf dem
                  Bildschirm aus. */
            
            System.out.println(zahl1 + " - " + zahl2 + " = " + (zahl1-zahl2));
            System.out.println(zahl1 + " * " + zahl2 + " = " + zahl1*zahl2);
            System.out.println(zahl1 + " : " + zahl2 + " = " + zahl1/zahl2);
            

            /* 5. Ueberpruefen Sie, ob die beiden Ganzzahlen gleich sind
                  und geben Sie das Ergebnis auf dem Bildschirm aus. */
            
            System.out.println(zahl1 == zahl2);

            /* 6. Wenden Sie drei anderen Vergleichsoperatoren auf die Ganzzahlen an
                  und geben Sie das Ergebnis jeweils auf dem Bildschirm aus. */
            
            System.out.println(zahl1 != zahl2);
            System.out.println(zahl1 > zahl2);
            System.out.println(zahl1 < zahl2);
            

            /* 7. Ueberpruefen Sie, ob die beiden Ganzzahlen im  Interval [0;50] liegen
                  und geben Sie das Ergebnis auf dem Bildschirm aus. 
       
                  Tipp: Auch das geht nur mit Operatoren!
            */
            
            
            System.out.println(zahl1>= 0 && zahl1<=50);
            System.out.println(zahl2 >= 0 && zahl2 <= 50);

      } //Ende von main
} // Ende von Operatoren