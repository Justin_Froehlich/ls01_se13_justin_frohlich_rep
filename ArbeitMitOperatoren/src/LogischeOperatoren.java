/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 13.12.2021
  * @JustinFr�hlich
  */

public class LogischeOperatoren {
  
  public static void main(String[] args) {
    /* 1. Deklarieren Sie zwei Warheitswerte a und b.*/
	  
	  boolean a;
	  boolean b;
	  
    
    /* 2. Initialisieren Sie einen Wert mit true, den anderen mit false */
	  
	  a=true;
	  b=false;
    
    /* 3. Geben Sie beide Werte aus */
	  
	  System.out.println(a);
	  System.out.println(b);
    
	  
    /* 4. Deklarieren Sie einen Wahrheitswert undGatter */
	  
	  /*boolean c;*/
	  boolean undgatter;
	  
    
    /* 5. Weisen Sie der Variable undGatter den Wert "a AND b" zu 
          und geben Sie das Ergebnis aus. */
	  
	  undgatter = a & b;
	  System.out.println(undgatter);
	  
          
    /* 6. Deklarieren Sie au�erdem den Wahrheitswert c und initialisieren ihn
          direkt mit dem Wert true */
	  
	  boolean c = true;
          
    /* 7. Verkn�pfen Sie alle drei Wahrheitswerte a, b und c und geben Sie
          jeweils das Ergebnis aus */
       // a)  a AND b AND c
       // b)  a OR  b OR  c
       // c)  a AND b OR  c
       // d)  a OR  b AND c
       // e)  a XOR b AND c
       // f) (a XOR b) OR c
	  
	  System.out.println(a & b & c);
	  System.out.println(a || b || c);
	  System.out.println(a & b || c);
	  System.out.println(a || b & c);
	  System.out.println(a ^ b & c);
	  System.out.println((a ^ b) || c);
       
  } // end of main

} // end of class LogischeOperatoren
