import java.util.Scanner;

public class Version1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		Scanner tastatur = new Scanner(System.in);
		
		double zahl1;
		double zahl2;
		char operationszeichen;
		
		
		System.out.println("Wie lautet ihre erste Zahl?");
		zahl1 = tastatur.nextDouble();
		System.out.println("Welche Operation?");
		operationszeichen = tastatur.next().charAt(0);
		System.out.println("Wie lautet ihre zweite Zahl?");
		zahl2 = tastatur.nextDouble();
		
		double ergebnis = 0;
		
		if (operationszeichen == '+'){
			ergebnis = addition(zahl1,zahl2);
		}
		else if (operationszeichen == '-') {
			ergebnis = substraktion(zahl1,zahl2);
		}
		else if (operationszeichen == '*') {
			ergebnis = multiplikation(zahl1,zahl2);
		}
		else if (operationszeichen == '/' || operationszeichen == ':') {
			ergebnis = division(zahl1,zahl2);
		}
		else {
			System.out.println("Falsche Eingabe");
		}

		
		System.out.println("Das Ergebnis lautet: " + ergebnis);
		
		tastatur.close();
	}
	public static double addition(double a,double b) {
		return a+b;
	}
	public static double substraktion(double a,double b) {
		return a-b;
	}
	public static double multiplikation(double a,double b) {
		return a*b;
	}
	public static double division(double a,double b) {
		return a/b;
	}
	
}
