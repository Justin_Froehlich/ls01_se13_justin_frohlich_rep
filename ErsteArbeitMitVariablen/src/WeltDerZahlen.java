/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Justin Fr�hlich >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8;
    
    // Anzahl der Sterne in unserer Milchstra�e
    long anzahlSterne = 150000000000L;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3645000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    short alterTage = 6759;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 150000; 
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
    int flaecheGroessteLand = 17098242;
    
    // Wie gro� ist das kleinste Land der Erde?
    
    double flaecheKleinsteLand = 0.44;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzahl der Planeten in unserem Sonnensystem: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne in unserer Milchstra�e: " + anzahlSterne);
    
    System.out.println("Berlin hat " + bewohnerBerlin + " Einwohner");
    
    System.out.println("Ich bin " + alterTage + " Tage alt.");
    
    System.out.println("Das schwerste Tier der Erde, der Blauwal, wiegt " + gewichtKilogramm + " Kilogramm");
    
    System.out.println("Russland ist das gr��te Land, mit einer Fl�che von: " + flaecheGroessteLand + " quadrazkilometer");
    
    System.out.println("Vatikanstadt ist das kleine Land, mit einer Fl�che von :" + flaecheKleinsteLand + " quadratkilometer");
    
    
    
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

